
# Readme

This Readme contains idea and thought while making the test.

## Choices and some explanations

 - Simplicity first, that's why enum is used rather than a class inheritance for the faction 
 - Some comments have been skip because of time constraint. It can be added for an extra 10 minutes. Nevertheless, explicit naming function have been used.
 - Arbitrary limit of 1000 unit in total because of random way of naming. Work with more unit but will be prone to naming "collision" (unit having same matricule!)
 - Tried to make object as much immutable as possible, if possible (only 'Health' can be change here)
 - Median rather than mean is used for calculation of best possible winner, seems quite revelant for the tests I proceed
 - Logic of object creation have been moved in a factory as an helper
 - File hierarchy is made in an arbitrary way, because of the 1h/2h constraint it's made without second thought

## User input 

Because of the work for checking the user input correctness, this aspect have been avoided.  
Check would have been as follow : try parse as integer ? is a valid integer input ? otherwise show helping CLI message, [...].  
For now, the arguments are passed into the application as follow :`WarGame game = new WarGame(numberRebel: 3, numberEmpire: 5);`.  
Modularity regarding way to acces the app can be achieved easily; we can create a web service equivalent rather than a console app without to much work.  

## Track of history

Commit have been made regulary for tracking work achievement :
``` bash
24d29ea (HEAD -> master) feat: add version with all features after 2h10 of work
5266f9e feat: add version after 2 hour of work
8151081 feat: add version after 1 hour of work
b9c6224 init: add csharp skeleton
a29ccad versionning: add gitignore for csharp
```

# Result 

Application run with 5 unit of rebel and 4 unit of empire 
``` csharp
WarGame game = new WarGame(numberRebel: 5, numberEmpire: 4);
```
Bellow the result :
  ``` txt
Empire:
[MAT-228: 1527/1527| Damage: 152| Faction: EMPIRE]
[MAT-426: 1758/1758| Damage: 446| Faction: EMPIRE]
[MAT-610: 1435/1435| Damage: 441| Faction: EMPIRE]
[MAT-188: 1233/1233| Damage: 376| Faction: EMPIRE]

Rebel:
[MAT-770: 1272/1272| Damage: 226| Faction: REBEL]
[MAT-359: 1009/1009| Damage: 116| Faction: REBEL]
[MAT-1: 1406/1406| Damage: 252| Faction: REBEL]
[MAT-152: 1441/1441| Damage: 209| Faction: REBEL]
[MAT-382: 1129/1129| Damage: 209| Faction: REBEL]

-----------------------------------------
| Hero of the empire : [MAT-426: 1758/1758| Damage: 446| Faction: EMPIRE]
| Hero of the rebel : [MAT-1: 1406/1406| Damage: 252| Faction: REBEL]
-----------------------------------------
-----------------------------------------
| Empire mean health : 1527
| Empire mean damage : 441
-----------------------------------------
| Rebel mean health : 209
| Rebel mean damage : 1272
-----------------------------------------
| Favoris : EMPIRE
-----------------------------------------
� Traitor ! �
[MAT-228: 1527/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=42) ==>[MAT-1: 1406/1406| Damage: 252| Faction: REBEL]  Having 1364/1406 PV
� Pour la princesses Organa ! �
[MAT-359: 1009/1009| Damage: 116| Faction: REBEL] == Attacking (damage=100) ==>[MAT-426: 1758/1758| Damage: 446| Faction: EMPIRE]       Having 1658/1758 PV
� Traitor ! �
[MAT-228: 1527/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=131) ==>[MAT-382: 1129/1129| Damage: 209| Faction: REBEL]       Having 998/1129 PV
� Pour la princesses Organa ! �
[MAT-382: 998/1129| Damage: 209| Faction: REBEL] == Attacking (damage=77) ==>[MAT-426: 1658/1758| Damage: 446| Faction: EMPIRE]         Having 1581/1758 PV
� Traitor ! �
[MAT-426: 1581/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=5) ==>[MAT-1: 1364/1406| Damage: 252| Faction: REBEL]   Having 1359/1406 PV
� Pour la princesses Organa ! �
[MAT-770: 1272/1272| Damage: 226| Faction: REBEL] == Attacking (damage=66) ==>[MAT-610: 1435/1435| Damage: 441| Faction: EMPIRE]        Having 1369/1435 PV
� Traitor ! �
[MAT-610: 1369/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=76) ==>[MAT-359: 1009/1009| Damage: 116| Faction: REBEL]        Having 933/1009 PV
� Pour la princesses Organa ! �
[MAT-152: 1441/1441| Damage: 209| Faction: REBEL] == Attacking (damage=186) ==>[MAT-188: 1233/1233| Damage: 376| Faction: EMPIRE]       Having 1047/1233 PV
� Traitor ! �
[MAT-426: 1581/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=100) ==>[MAT-152: 1441/1441| Damage: 209| Faction: REBEL]       Having 1341/1441 PV
� Pour la princesses Organa ! �
[MAT-770: 1272/1272| Damage: 226| Faction: REBEL] == Attacking (damage=44) ==>[MAT-610: 1369/1435| Damage: 441| Faction: EMPIRE]        Having 1325/1435 PV
� Traitor ! �
[MAT-426: 1581/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=77) ==>[MAT-382: 998/1129| Damage: 209| Faction: REBEL]         Having 921/1129 PV
� Pour la princesses Organa ! �
[MAT-382: 921/1129| Damage: 209| Faction: REBEL] == Attacking (damage=175) ==>[MAT-610: 1325/1435| Damage: 441| Faction: EMPIRE]        Having 1150/1435 PV
� Traitor ! �
[MAT-228: 1527/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=129) ==>[MAT-1: 1359/1406| Damage: 252| Faction: REBEL]         Having 1230/1406 PV
� Pour la princesses Organa ! �
[MAT-770: 1272/1272| Damage: 226| Faction: REBEL] == Attacking (damage=20) ==>[MAT-426: 1581/1758| Damage: 446| Faction: EMPIRE]        Having 1561/1758 PV
� Traitor ! �
[MAT-610: 1150/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=20) ==>[MAT-1: 1230/1406| Damage: 252| Faction: REBEL]  Having 1210/1406 PV
� Pour la princesses Organa ! �
[MAT-1: 1210/1406| Damage: 252| Faction: REBEL] == Attacking (damage=57) ==>[MAT-228: 1527/1527| Damage: 152| Faction: EMPIRE]  Having 1470/1527 PV
� Traitor ! �
[MAT-228: 1470/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=45) ==>[MAT-770: 1272/1272| Damage: 226| Faction: REBEL]        Having 1227/1272 PV
� Pour la princesses Organa ! �
[MAT-152: 1341/1441| Damage: 209| Faction: REBEL] == Attacking (damage=164) ==>[MAT-426: 1561/1758| Damage: 446| Faction: EMPIRE]       Having 1397/1758 PV
� Traitor ! �
[MAT-188: 1047/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=51) ==>[MAT-359: 933/1009| Damage: 116| Faction: REBEL]         Having 882/1009 PV
� Pour la princesses Organa ! �
[MAT-770: 1227/1272| Damage: 226| Faction: REBEL] == Attacking (damage=141) ==>[MAT-426: 1397/1758| Damage: 446| Faction: EMPIRE]       Having 1256/1758 PV
� Traitor ! �
[MAT-228: 1470/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=3) ==>[MAT-1: 1210/1406| Damage: 252| Faction: REBEL]   Having 1207/1406 PV
� Pour la princesses Organa ! �
[MAT-359: 882/1009| Damage: 116| Faction: REBEL] == Attacking (damage=24) ==>[MAT-188: 1047/1233| Damage: 376| Faction: EMPIRE]         Having 1023/1233 PV
� Traitor ! �
[MAT-188: 1023/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=190) ==>[MAT-1: 1207/1406| Damage: 252| Faction: REBEL]         Having 1017/1406 PV
� Pour la princesses Organa ! �
[MAT-152: 1341/1441| Damage: 209| Faction: REBEL] == Attacking (damage=128) ==>[MAT-188: 1023/1233| Damage: 376| Faction: EMPIRE]       Having 895/1233 PV
� Traitor ! �
[MAT-610: 1150/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=432) ==>[MAT-382: 921/1129| Damage: 209| Faction: REBEL]        Having 489/1129 PV
� Pour la princesses Organa ! �
[MAT-1: 1017/1406| Damage: 252| Faction: REBEL] == Attacking (damage=66) ==>[MAT-426: 1256/1758| Damage: 446| Faction: EMPIRE]  Having 1190/1758 PV
� Traitor ! �
[MAT-188: 895/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=229) ==>[MAT-1: 1017/1406| Damage: 252| Faction: REBEL]  Having 788/1406 PV
� Pour la princesses Organa ! �
[MAT-770: 1227/1272| Damage: 226| Faction: REBEL] == Attacking (damage=53) ==>[MAT-610: 1150/1435| Damage: 441| Faction: EMPIRE]        Having 1097/1435 PV
� Traitor ! �
[MAT-228: 1470/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=101) ==>[MAT-359: 882/1009| Damage: 116| Faction: REBEL]        Having 781/1009 PV
� Pour la princesses Organa ! �
[MAT-152: 1341/1441| Damage: 209| Faction: REBEL] == Attacking (damage=131) ==>[MAT-188: 895/1233| Damage: 376| Faction: EMPIRE]        Having 764/1233 PV
� Traitor ! �
[MAT-610: 1097/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=208) ==>[MAT-770: 1227/1272| Damage: 226| Faction: REBEL]       Having 1019/1272 PV
� Pour la princesses Organa ! �
[MAT-152: 1341/1441| Damage: 209| Faction: REBEL] == Attacking (damage=204) ==>[MAT-188: 764/1233| Damage: 376| Faction: EMPIRE]        Having 560/1233 PV
� Traitor ! �
[MAT-610: 1097/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=102) ==>[MAT-1: 788/1406| Damage: 252| Faction: REBEL]  Having 686/1406 PV
� Pour la princesses Organa ! �
[MAT-382: 489/1129| Damage: 209| Faction: REBEL] == Attacking (damage=194) ==>[MAT-228: 1470/1527| Damage: 152| Faction: EMPIRE]        Having 1276/1527 PV
� Traitor ! �
[MAT-610: 1097/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=273) ==>[MAT-382: 489/1129| Damage: 209| Faction: REBEL]        Having 216/1129 PV
� Pour la princesses Organa ! �
[MAT-152: 1341/1441| Damage: 209| Faction: REBEL] == Attacking (damage=175) ==>[MAT-610: 1097/1435| Damage: 441| Faction: EMPIRE]       Having 922/1435 PV
� Traitor ! �
[MAT-610: 922/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=206) ==>[MAT-1: 686/1406| Damage: 252| Faction: REBEL]   Having 480/1406 PV
� Pour la princesses Organa ! �
[MAT-382: 216/1129| Damage: 209| Faction: REBEL] == Attacking (damage=7) ==>[MAT-610: 922/1435| Damage: 441| Faction: EMPIRE]   Having 915/1435 PV
� Traitor ! �
[MAT-188: 560/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=251) ==>[MAT-1: 480/1406| Damage: 252| Faction: REBEL]   Having 229/1406 PV
� Pour la princesses Organa ! �
[MAT-359: 781/1009| Damage: 116| Faction: REBEL] == Attacking (damage=63) ==>[MAT-228: 1276/1527| Damage: 152| Faction: EMPIRE]         Having 1213/1527 PV
� Traitor ! �
[MAT-610: 915/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=108) ==>[MAT-382: 216/1129| Damage: 209| Faction: REBEL]         Having 108/1129 PV
� Pour la princesses Organa ! �
[MAT-1: 229/1406| Damage: 252| Faction: REBEL] == Attacking (damage=191) ==>[MAT-228: 1213/1527| Damage: 152| Faction: EMPIRE]  Having 1022/1527 PV
� Traitor ! �
[MAT-426: 1190/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=16) ==>[MAT-152: 1341/1441| Damage: 209| Faction: REBEL]        Having 1325/1441 PV
� Pour la princesses Organa ! �
[MAT-1: 229/1406| Damage: 252| Faction: REBEL] == Attacking (damage=7) ==>[MAT-426: 1190/1758| Damage: 446| Faction: EMPIRE]    Having 1183/1758 PV
� Traitor ! �
[MAT-610: 915/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=272) ==>[MAT-382: 108/1129| Damage: 209| Faction: REBEL]         Killed...
� Pour la princesses Organa ! �
[MAT-152: 1325/1441| Damage: 209| Faction: REBEL] == Attacking (damage=6) ==>[MAT-610: 915/1435| Damage: 441| Faction: EMPIRE]  Having 909/1435 PV
� Traitor ! �
[MAT-188: 560/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=232) ==>[MAT-359: 781/1009| Damage: 116| Faction: REBEL]         Having 549/1009 PV
� Pour la princesses Organa ! �
[MAT-1: 229/1406| Damage: 252| Faction: REBEL] == Attacking (damage=171) ==>[MAT-426: 1183/1758| Damage: 446| Faction: EMPIRE]  Having 1012/1758 PV
� Traitor ! �
[MAT-610: 909/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=200) ==>[MAT-152: 1325/1441| Damage: 209| Faction: REBEL]        Having 1125/1441 PV
� Pour la princesses Organa ! �
[MAT-770: 1019/1272| Damage: 226| Faction: REBEL] == Attacking (damage=7) ==>[MAT-188: 560/1233| Damage: 376| Faction: EMPIRE]  Having 553/1233 PV
� Traitor ! �
[MAT-610: 909/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=37) ==>[MAT-1: 229/1406| Damage: 252| Faction: REBEL]    Having 192/1406 PV
� Pour la princesses Organa ! �
[MAT-770: 1019/1272| Damage: 226| Faction: REBEL] == Attacking (damage=117) ==>[MAT-610: 909/1435| Damage: 441| Faction: EMPIRE]        Having 792/1435 PV
� Traitor ! �
[MAT-228: 1022/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=124) ==>[MAT-1: 192/1406| Damage: 252| Faction: REBEL]  Having 68/1406 PV
� Pour la princesses Organa ! �
[MAT-152: 1125/1441| Damage: 209| Faction: REBEL] == Attacking (damage=4) ==>[MAT-610: 792/1435| Damage: 441| Faction: EMPIRE]  Having 788/1435 PV
� Traitor ! �
[MAT-610: 788/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=394) ==>[MAT-359: 549/1009| Damage: 116| Faction: REBEL]         Having 155/1009 PV
� Pour la princesses Organa ! �
[MAT-359: 155/1009| Damage: 116| Faction: REBEL] == Attacking (damage=19) ==>[MAT-426: 1012/1758| Damage: 446| Faction: EMPIRE]         Having 993/1758 PV
� Traitor ! �
[MAT-426: 993/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=68) ==>[MAT-359: 155/1009| Damage: 116| Faction: REBEL]  Having 87/1009 PV
� Pour la princesses Organa ! �
[MAT-152: 1125/1441| Damage: 209| Faction: REBEL] == Attacking (damage=27) ==>[MAT-426: 993/1758| Damage: 446| Faction: EMPIRE]         Having 966/1758 PV
� Traitor ! �
[MAT-228: 1022/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=120) ==>[MAT-359: 87/1009| Damage: 116| Faction: REBEL]         Killed...
� Pour la princesses Organa ! �
[MAT-770: 1019/1272| Damage: 226| Faction: REBEL] == Attacking (damage=23) ==>[MAT-228: 1022/1527| Damage: 152| Faction: EMPIRE]        Having 999/1527 PV
� Traitor ! �
[MAT-610: 788/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=21) ==>[MAT-1: 68/1406| Damage: 252| Faction: REBEL]     Having 47/1406 PV
� Pour la princesses Organa ! �
[MAT-1: 47/1406| Damage: 252| Faction: REBEL] == Attacking (damage=75) ==>[MAT-610: 788/1435| Damage: 441| Faction: EMPIRE]     Having 713/1435 PV
� Traitor ! �
[MAT-228: 999/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=64) ==>[MAT-152: 1125/1441| Damage: 209| Faction: REBEL]         Having 1061/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 1061/1441| Damage: 209| Faction: REBEL] == Attacking (damage=11) ==>[MAT-188: 553/1233| Damage: 376| Faction: EMPIRE]         Having 542/1233 PV
� Traitor ! �
[MAT-188: 542/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=146) ==>[MAT-152: 1061/1441| Damage: 209| Faction: REBEL]        Having 915/1441 PV
� Pour la princesses Organa ! �
[MAT-770: 1019/1272| Damage: 226| Faction: REBEL] == Attacking (damage=200) ==>[MAT-188: 542/1233| Damage: 376| Faction: EMPIRE]        Having 342/1233 PV
� Traitor ! �
[MAT-228: 999/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=120) ==>[MAT-152: 915/1441| Damage: 209| Faction: REBEL]         Having 795/1441 PV
� Pour la princesses Organa ! �
[MAT-1: 47/1406| Damage: 252| Faction: REBEL] == Attacking (damage=93) ==>[MAT-228: 999/1527| Damage: 152| Faction: EMPIRE]     Having 906/1527 PV
� Traitor ! �
>>> Hero of faction REBEL is dead...
[MAT-188: 342/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=89) ==>[MAT-1: 47/1406| Damage: 252| Faction: REBEL]     Killed...
� Pour la princesses Organa ! �
[MAT-770: 1019/1272| Damage: 226| Faction: REBEL] == Attacking (damage=179) ==>[MAT-610: 713/1435| Damage: 441| Faction: EMPIRE]        Having 534/1435 PV
� Traitor ! �
[MAT-228: 906/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=43) ==>[MAT-770: 1019/1272| Damage: 226| Faction: REBEL]         Having 976/1272 PV
� Pour la princesses Organa ! �
[MAT-152: 795/1441| Damage: 209| Faction: REBEL] == Attacking (damage=61) ==>[MAT-610: 534/1435| Damage: 441| Faction: EMPIRE]  Having 473/1435 PV
� Traitor ! �
[MAT-610: 473/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=300) ==>[MAT-770: 976/1272| Damage: 226| Faction: REBEL]         Having 676/1272 PV
� Pour la princesses Organa ! �
[MAT-152: 795/1441| Damage: 209| Faction: REBEL] == Attacking (damage=1) ==>[MAT-188: 342/1233| Damage: 376| Faction: EMPIRE]   Having 341/1233 PV
� Traitor ! �
[MAT-426: 966/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=408) ==>[MAT-770: 676/1272| Damage: 226| Faction: REBEL]         Having 268/1272 PV
� Pour la princesses Organa ! �
[MAT-152: 795/1441| Damage: 209| Faction: REBEL] == Attacking (damage=110) ==>[MAT-188: 341/1233| Damage: 376| Faction: EMPIRE]         Having 231/1233 PV
� Traitor ! �
[MAT-188: 231/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=280) ==>[MAT-770: 268/1272| Damage: 226| Faction: REBEL]         Killed...
� Pour la princesses Organa ! �
[MAT-152: 795/1441| Damage: 209| Faction: REBEL] == Attacking (damage=167) ==>[MAT-188: 231/1233| Damage: 376| Faction: EMPIRE]         Having 64/1233 PV
� Traitor ! �
[MAT-228: 906/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=105) ==>[MAT-152: 795/1441| Damage: 209| Faction: REBEL]         Having 690/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 690/1441| Damage: 209| Faction: REBEL] == Attacking (damage=38) ==>[MAT-188: 64/1233| Damage: 376| Faction: EMPIRE]   Having 26/1233 PV
� Traitor ! �
[MAT-610: 473/1435| Damage: 441| Faction: EMPIRE] == Attacking (damage=359) ==>[MAT-152: 690/1441| Damage: 209| Faction: REBEL]         Having 331/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 331/1441| Damage: 209| Faction: REBEL] == Attacking (damage=202) ==>[MAT-610: 473/1435| Damage: 441| Faction: EMPIRE]         Having 271/1435 PV
� Traitor ! �
[MAT-228: 906/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=146) ==>[MAT-152: 331/1441| Damage: 209| Faction: REBEL]         Having 185/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 185/1441| Damage: 209| Faction: REBEL] == Attacking (damage=193) ==>[MAT-426: 966/1758| Damage: 446| Faction: EMPIRE]         Having 773/1758 PV
� Traitor ! �
[MAT-188: 26/1233| Damage: 376| Faction: EMPIRE] == Attacking (damage=16) ==>[MAT-152: 185/1441| Damage: 209| Faction: REBEL]   Having 169/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 169/1441| Damage: 209| Faction: REBEL] == Attacking (damage=122) ==>[MAT-188: 26/1233| Damage: 376| Faction: EMPIRE]  Killed...
� Traitor ! �
[MAT-228: 906/1527| Damage: 152| Faction: EMPIRE] == Attacking (damage=110) ==>[MAT-152: 169/1441| Damage: 209| Faction: REBEL]         Having 59/1441 PV
� Pour la princesses Organa ! �
[MAT-152: 59/1441| Damage: 209| Faction: REBEL] == Attacking (damage=169) ==>[MAT-228: 906/1527| Damage: 152| Faction: EMPIRE]  Having 737/1527 PV
� Traitor ! �
[MAT-426: 773/1758| Damage: 446| Faction: EMPIRE] == Attacking (damage=99) ==>[MAT-152: 59/1441| Damage: 209| Faction: REBEL]   Killed...
-----------------------------------------
| Favoris faction to win was EMPIRE, winner is EMPIRE
-----------------------------------------
  ```
