﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TEST_POO_CSharp_N0.Game.Model;

namespace TEST_POO_CSharp_N0.Game
{
    internal class WarGame
    {
        private List<Warrior> rebelArmy;
        private List<Warrior> empireArmy;
        private Faction currentPlayingFaction;
        private WarriorFactory factory = new();

        private Faction suspectedWinner;
        private Warrior rebelHero;
        private Warrior empireHero;

        public WarGame(int numberRebel, int numberEmpire,Faction startingFaction = Faction.REBEL)
        {
            this.rebelArmy = factory.GenerateRebelArmy(numberRebel);
            this.empireArmy = factory.GenerateEmpireArmy(numberEmpire);
            this.currentPlayingFaction = startingFaction;
        }

        public void Run()
        {
            // Initialise variable
            List<Warrior> attackingArmy;
            List<Warrior> targetedArmy;
            Warrior attackingWarrior;
            Warrior targetedWarrior;

            Random rand = new Random();

            while (CanContinuGame())
            {
                // Set the next playing faction for this turn
                currentPlayingFaction = currentPlayingFaction is Faction.REBEL ? Faction.EMPIRE : Faction.REBEL;

                // Choose the attacking army
                if (currentPlayingFaction is Faction.EMPIRE)
                {
                    attackingArmy = empireArmy;
                    targetedArmy = rebelArmy;
                }
                else if(currentPlayingFaction is Faction.REBEL)
                {
                    attackingArmy = rebelArmy ;
                    targetedArmy = empireArmy;
                }
                else throw new InvalidEnumArgumentException($"Enum 'Faction' is not in the allowed values. Current is {currentPlayingFaction}");

                // Choose random unit for attack
                attackingWarrior = attackingArmy.ElementAt(rand.Next(0,attackingArmy.Count));
                targetedWarrior = targetedArmy.ElementAt(rand.Next(0, targetedArmy.Count));

                // Yield a sentence before attacking
                if (attackingWarrior.Faction is Faction.REBEL) Console.WriteLine("« Pour la princesses Organa ! »");
                else if(attackingWarrior.Faction is Faction.EMPIRE) Console.WriteLine("« Traitor ! »");

                // Attacking
                var assultSummary = attackingWarrior.Assault(targetedWarrior);

                // Remove the dead body
                if (targetedWarrior.IsDead()) targetedArmy.Remove(targetedWarrior);

                // Annonce death of the hero
                if (targetedWarrior.IsDead() && (targetedWarrior.Equals(rebelHero) || targetedWarrior.Equals(empireHero))){
                    Console.WriteLine($">>> Hero of faction {targetedWarrior.Faction} is dead...");
                }
                //Show summary
                Console.WriteLine(assultSummary);
            }
        }

        protected bool CanContinuGame() => rebelArmy.Count > 0 && empireArmy.Count > 0;

        internal void AnnonceSuspectedWinner()
        {
            // Calculate median of key values for rebels
            int medianDamageRebel = rebelArmy.Select(el => el.Damage).Order().ElementAt(rebelArmy.Count / 2);
            int medianHealthRebel = rebelArmy.Select(el => el.Health).Order().ElementAt(rebelArmy.Count / 2);

            // Calculate median of key values for empire
            int medianDamageEmpire = empireArmy.Select(el => el.Damage).Order().ElementAt(rebelArmy.Count / 2);
            int medianHealthEmpire = empireArmy.Select(el => el.Health).Order().ElementAt(rebelArmy.Count / 2);

            // Calculate more advantaged army as the median soldier having the most health after an enemy attack
            if ((medianHealthEmpire - medianDamageRebel) >= (medianHealthRebel - medianDamageEmpire)) suspectedWinner = Faction.EMPIRE;
            else suspectedWinner = Faction.REBEL;

            Console.WriteLine($"""
                -----------------------------------------
                | Empire mean health : {medianHealthEmpire}
                | Empire mean damage : {medianDamageEmpire}
                -----------------------------------------
                | Rebel mean health : {medianDamageRebel}
                | Rebel mean damage : {medianHealthRebel}
                -----------------------------------------
                | Favoris : {suspectedWinner}
                -----------------------------------------
                """);
        }

        internal void CheckSuspectedWinner()
        {
            Faction winner = rebelArmy.Count() is 0 ? Faction.EMPIRE : Faction.REBEL;
            Console.WriteLine($"""
                -----------------------------------------
                | Favoris faction to win was {suspectedWinner}, winner is {winner}
                -----------------------------------------
                """);
        }

        internal void ShowArmy()
        {
            Console.WriteLine("Empire:");
            string summary = "";
            foreach (var element in empireArmy) Console.WriteLine(element);
            Console.WriteLine(summary);

            Console.WriteLine("Rebel:");
            foreach (var element in rebelArmy) Console.WriteLine(element);
            Console.WriteLine(summary);

        }

        internal void ShowHeroes()
        {
            rebelHero = rebelArmy.MaxBy(el => el.MaxHealth + el.Damage * 10)!;
            empireHero = empireArmy.MaxBy(el => el.MaxHealth + el.Damage * 10)!;

            Console.WriteLine($"""
                -----------------------------------------
                | Hero of the empire : {empireHero}
                | Hero of the rebel : {rebelHero}
                -----------------------------------------
                """);
        }
    }
}
