﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEST_POO_CSharp_N0.Game.Model;

namespace TEST_POO_CSharp_N0.Game
{
    public class WarriorFactory
    {
        public WarriorFactory() { }
        public Warrior GenerateRandomRebel() => GenerateRandomWarrior(Faction.REBEL);
        public Warrior GenerateRandomEmpire() => GenerateRandomWarrior(Faction.EMPIRE);
        public List<Warrior> GenerateRebelArmy(int unitNumber) => GenerateArmy(unitNumber, Faction.REBEL);
        public List<Warrior> GenerateEmpireArmy(int unitNumber) => GenerateArmy(unitNumber,Faction.EMPIRE);

        protected Warrior GenerateRandomWarrior(Faction faction)
        {
            Random random = new Random();
            string name = $"MAT-{random.NextInt64(0,1_000)}";
            ushort health = (ushort)(random.Next(1_000, 2_000));
            ushort damage = (ushort)(random.Next(100, 500));
            return new(health, damage, name, faction);
        }

        protected List<Warrior> GenerateArmy(int unitNumber,Faction faction)
        {
            var elements = new List<Warrior>();
            for(int unit=0; unit < unitNumber; unit++)
            {
                elements.Add(GenerateRandomWarrior(faction));
            }
            return elements;
        }
    }
}
