﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEST_POO_CSharp_N0.Game.Model
{
    public class Warrior
    {
        public int Health { get; set; }
        public int MaxHealth { get; init; }
        public int Damage { get; init; }
        public string Name { get; init; }
        public Faction Faction { get; init; }
        public Warrior(int health, int damage, string name, Faction faction)
        {
            Health = health;
            MaxHealth = Health;
            Damage = damage;
            Name = name;
            Faction = faction;
        }


        public string Assault(Warrior anotherWarrior)
        {
            Random rand = new Random();

            // Random percent of damage
            int moduledDamage =  (int)(rand.NextSingle() * this.Damage);

            string summary = $"{this} == Attacking (damage={moduledDamage}) ==>{anotherWarrior} \t";

            anotherWarrior.Health = anotherWarrior.Health - moduledDamage;

            if (anotherWarrior.IsDead()) summary += "Killed...";
            else summary += $"Having {anotherWarrior.Health}/{anotherWarrior.MaxHealth} PV";

            return summary;
        }

        public bool IsDead() => Health <= 0;
        public override string ToString()
        {
            return $"[{Name}: {Health}/{MaxHealth}| Damage: {Damage}| Faction: {Faction}]";
        }
    }
}
