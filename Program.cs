﻿using TEST_POO_CSharp_N0.Game;

namespace TEST_POO_CSharp_N0
{
    internal class Program
    {
        static void Main(string[] args)
        {

            WarGame game = new WarGame(numberRebel: 1, numberEmpire: 1);
            
            game.ShowArmy();
            game.ShowHeroes();
            game.AnnonceSuspectedWinner();

            game.Run();

            game.CheckSuspectedWinner();
        }
    }
}